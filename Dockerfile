FROM python:3-alpine

WORKDIR /usr/src/app
RUN pip3 install flask
COPY logger.py /usr/src/app

CMD ["python3", "logger.py"]