## Echo

A slim python HTTP server that echos back what was sent to it. And logs it.

Used to log requests intercepted by kong plugins

