#!/usr/bin/env python3
from flask import Flask, request, jsonify
import os, json, logging

from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

PORT = os.environ.get("PORT", 5001)
app = Flask(__name__)

@app.errorhandler(404)
def page_not_found(error):
    data = {}
    data['method'] = request.method
    data['path'] = request.path
    data['params'] = dict(request.args)
    data['source'] = request.remote_addr

    if request.is_json:
        data['body'] = dict(request.get_json(force=True))

    data['headers'] = dict(request.headers)
    app.logger.info(data)
    return jsonify(data), 200

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=PORT)